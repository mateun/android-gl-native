//
// Created by mgrus on 18/12/2021.
//

#include <jni.h>
#include <android/log.h>
#include <GLES3/gl3.h>

extern "C" {
    JNIEXPORT void JNICALL Java_com_example_myapplication_GLES3JNILib_init(JNIEnv *env, jclass obj);
    JNIEXPORT void JNICALL
    Java_com_example_myapplication_GLES3JNILib_resize(JNIEnv *env, jclass obj, jint width,
                                                      jint height);
    JNIEXPORT void JNICALL Java_com_example_myapplication_GLES3JNILib_render(JNIEnv *env, jclass obj);

}

extern "C" JNIEXPORT void JNICALL
Java_com_example_myapplication_GLES3JNILib_init(JNIEnv *env, jclass obj) {
    __android_log_write(ANDROID_LOG_INFO, "MYTAG", "in gl init");
    glClearColor(1, 0, 0, 1);
}


extern "C"
JNIEXPORT void JNICALL
Java_com_example_myapplication_GLES3JNILib_resize(JNIEnv *env, jclass clazz, jint width,
                                                  jint height) {
    // TODO: implement resize()
    __android_log_write(ANDROID_LOG_INFO, "MYTAG", "in resize");
}
extern "C"
JNIEXPORT void JNICALL
Java_com_example_myapplication_GLES3JNILib_render(JNIEnv *env, jclass clazz) {
    // TODO: implement render()
    glClear(GL_COLOR_BUFFER_BIT);
}