//
// Created by mgrus on 18/12/2021.
//

#include "game-activity/GameActivity.cpp"
#include <game-text-input/gametextinput.cpp>
#include <android/log.h>
#include "game-activity/native_app_glue/android_native_app_glue.h"


extern "C" {
void android_main(struct android_app* state);
};

void _cmd_proxy(struct android_app * app,int32_t) {
    // handle the commands here!
}

void android_main(struct android_app* app) {
    __android_log_write(ANDROID_LOG_INFO, "MYTAG", "in android main");
    app->onAppCmd =_cmd_proxy;
    while (1) {
        __android_log_write(ANDROID_LOG_INFO, "MYTAG", "in main game loop");
    }
}