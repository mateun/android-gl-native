//
// Created by mgrus on 17/12/2021.
//

#include "RockGame.h"

#include <jni.h>

extern "C" {
    JNIEXPORT int JNICALL Java_com_example_myapplication_RenderWrapper_add(JNIEnv* env, jclass obj, jint width, jint height);
};

extern "C" JNIEXPORT
int JNICALL Java_com_example_myapplication_RenderWrapper_add(JNIEnv* env, jclass obj, jint width, jint height) {
    return width + height;
}