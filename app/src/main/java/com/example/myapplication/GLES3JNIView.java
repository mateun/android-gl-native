package com.example.myapplication;

import android.content.Context;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * also see:
 * https://github.com/quink-black/gles3-android/blob/master/app/src/main/java/com/android/gles3jni/GLES3JNIActivity.java
 */
public class GLES3JNIView extends GLSurfaceView {
    public GLES3JNIView(Context context) {
        super(context);
        init();
    }

    private void init() {
        setEGLConfigChooser(8, 8, 8, 8, 16, 0);
        setEGLContextClientVersion(3);
        setRenderer(new Renderer());
    }

    private class Renderer implements GLSurfaceView.Renderer {

        @Override
        public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
            System.out.println("on surface created!");
            GLES3JNILib.init();
        }

        @Override
        public void onSurfaceChanged(GL10 gl10, int w, int h) {
            GLES3JNILib.resize(w, h);
        }

        @Override
        public void onDrawFrame(GL10 gl10) {
            GLES3JNILib.render();
        }
    }
}
